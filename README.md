# pug-parser

The pug parser (takes an array of tokens and converts it to an abstract syntax tree)

[![Build Status](https://img.shields.io/travis/pugjs/pug-parser/master.svg)](https://travis-ci.org/pugjs/pug-parser)
[![Dependency Status](https://img.shields.io/david/pugjs/pug-parser.svg)](https://david-dm.org/pugjs/pug-parser)
[![NPM version](https://img.shields.io/npm/v/pug-parser.svg)](https://www.npmjs.org/package/pug-parser)

## Installation

    npm install pug-parser

## License

  MIT
